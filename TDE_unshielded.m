function [y] = TDE_unshielded(x)
    
    %transcendental equation for a sphere made of a ferromegnetic material
    %located in the free space

    global H0r R epsf k alpha gam Ms

    r1 = x(1);
    r2 = x(2);
    omc = r1 + 1i*r2;
    %renormalization
    f = omc*PhysConst.c/(2*pi);
    fm = gam*Ms*1e6;
    w = f/fm;
    %low-loss assumption
    denom = (H0r.^2-w.^2)+2*1i*alpha*w.*H0r;
    num1 = H0r+1i*alpha*w;
    num2 = w;
    mi1r = 1 + num1./denom;
    kap1 = num2./denom;
    mu = mi1r+kap1;
    x0 = omc*R;
    x = x0.*sqrt(epsf*mu);
    af1 = k - 0.5;
    af2 = k + 0.5;
    b1 = besselj(af1,x);
    b2 = besselj(af2,x);
    h1 = besselh(af1,2,x0);
    h2 = besselh(af2,2,x0);
    v1 = (k*b2 - x.*b1).*h2;
    v2 = mu.*(k.*h2 - x0.*h1).*b2;
    v = v1 - v2;
    y(1) = real(v);
    y(2) = imag(v);

end

