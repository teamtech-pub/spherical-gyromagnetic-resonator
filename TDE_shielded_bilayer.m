function y = TDE_shielded_bilayer(z)

    % Transcendental equation for a bilayer spherical resonator
    % loaded with a sphere made of a ferromegnetic material.
    
    global epsf epsd mu1 mu2 R1 R2 n H0r alpha omc Ms K g polarization
    anisotropy_factor = (6*K/Ms/Ms) / (4*pi);	
    %offset = -4/3*K/Ms; %anisotropy field for easy orientation
    offset_normalized = 0;
    sintheta2 = 2/3;
    xr = z(1);
    xi = z(2);
    x = xr+1i*xi;
    f = PhysConst.c.*x./(2*pi*R2);
    fm = PhysConst.gamGauss/PhysConst.ge*g*Ms*4*pi;
    w = f/fm;
    %low-loss assumption
    denom = ( (H0r+offset_normalized).^2-w.^2) + 2*1i*alpha*w.*(H0r+offset_normalized);
    li1 = (H0r+offset_normalized) + 1i*alpha*w;
    li2 = w;
    mu1r = 1+li1./denom;
    kap1 = li2./denom;
    denom_new = 1 + (mu1r - 1)*2*sintheta2*anisotropy_factor;
    kap1_new = kap1./denom_new;		
    mu1r_new = 1 + (mu1r-1 + ((mu1r-1).^2 - kap1.^2) * sintheta2 * anisotropy_factor)./denom_new;		
    mu1 = mu1r_new + polarization * kap1_new;
    t11 = sqrt(epsf.*mu1);
    t22 = sqrt(epsd.*mu2);
    omc = x./R2; %now omc = omega/c 
    k1 = omc.*t11;
    k2 = omc.*t22;
    x11 = k1.*R1;
    x21 = k2.*R1;
    x22 = k2.*R2;
    af1 = n-0.5;
    af2 = n+0.5;
    %Bessel function of 1st kind 
    j1x11 = besselj(af1,x11);
    j2x11 = besselj(af2,x11);
    j1x21 = besselj(af1,x21);
    j2x21 = besselj(af2,x21);
    y1x21 = bessely(af1,x21);
    y2x21 = bessely(af2,x21);
    %Bessel function of 2nd kind
    j2x22 = besselj(af2,x22);
    y2x22 = bessely(af2,x22);
    %matrix
    MT = zeros(3,3);
    MT(1,1) = mu1.*(x11).^0.5.*j2x11;
    MT(1,2) = -(x21).^0.5.*j2x21;
    MT(1,3) = -(x21).^0.5.*y2x21;
    MT(2,1) = (x11).^(3/2).*(j1x11-n.*j2x11./x11);
    MT(2,2) = -x21.^(3/2).*(j1x21-n.*j2x21./x21);
    MT(2,3) = -x21.^(3/2).*(y1x21-n.*y2x21./x21);
    MT(3,2) = (x22).^0.5.*j2x22;
    MT(3,3) = (x22).^0.5.*y2x22;
    y = abs((det(MT)));
    
end
