function y = mu_t(Hr,w,alpha)
    y = 1 + (Hr + 1i*alpha.*w) ./ (Hr.^2 - w.^2 + 2*1i*alpha.*Hr.*w); 
end
