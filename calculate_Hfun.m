function [zv] = calculate_Hfun(x)

    %Necessary for calculating the magnetic field integral using the Simpson method
    %The trancendental equation for a bilayer spherical resonator is used.
    
    global epsf epsd R1 R2 k mu1 mu2 omc
    N = length(x);
    t11 = sqrt(epsf.*mu1);
    t22 = sqrt(epsd.*mu2);
    k1 = omc.*t11;
    k2 = omc.*t22;
    af1 = k-0.5;
    af2 = k+0.5;
    x11 = k1.*R1;
    x21 = k2.*R1;
    x22 = k2.*R2;
    %first 
    j2x11 = besselj(af2,x11);
    j2x21 = besselj(af2,x21);
    y2x21 = bessely(af2,x21);
    %second
    j2x22 = besselj(af2,x22);
    y2x22 = bessely(af2,x22);
    %matrix
    A = mu1.*(k1.*R1).^0.5.*j2x11./((k2.*R1).^0.5.*(j2x21-j2x22./y2x22.*y2x21));	
    %H_r		
    z = zeros(1,N);		
    %H_theta		
    v = zeros(1,N);
    zv = zeros(1,N);
    
    for i=1:N
        ax = x(i);
        xi1 = k1.*ax;
        xi2 = k2.*ax;
        if (ax<=R1)
            ji = besselj(af2,xi1);
            ji1 = besselj(af1,xi1);
            z(i) = k1.^2.*k.*(k+1).*ji./(xi1.^(3/2));
            v(i) = k1.*(xi1).^0.5.*(ji1-k.*ji./xi1)./ax;
            %added a factor of 2 to H_theta^2, trig function missing in formulas 
            zv(i) = (ax*abs(z(i)))^2 + (1/k/(k+1))*(ax*abs(v(i)))^2;
        else
            jk = besselj(af2,xi2);
            jk1 = besselj(af1,xi2);
            yk = bessely(af2,xi2);
            yk1 = bessely(af1,xi2);
            w1 = jk1-k.*jk./xi2;
            w2 = yk1-k.*yk./xi2;
            z(i) = k2.^2.*k.*(k+1).*A.*(jk-j2x22./y2x22.*yk)./(xi2.^(3/2));
            v(i) = k2.*(xi2).^0.5.*A.*(w1-j2x22./y2x22.*w2)./ax;
            %added a factor of 2 to H_theta^2, trig function missing in formulas 
            zv(i) = (ax*abs(z(i)))^2 + (1/k/(k+1))*(ax*abs(v(i)))^2;
        end     
    end

end

