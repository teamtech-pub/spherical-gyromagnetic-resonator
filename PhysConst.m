classdef PhysConst
    %physical constants
    
   properties (Constant)
        %speed of light [m/s]
        c = 299792458; 
        %vacuum permeability [H/m]
        mu0 = 4*pi*1e-7;
        %vacuum permittivity [F/m]
        eps0 = 1/(4*pi*1e-7*299792458^2);
        %Bohr magneton J/T
        muB = 9.274009994e-24;
        %Planck constant J*s (2019 redefinition)
        h = 6.62607015e-34;
        %gyromagnetic ratio
        gamNIST = 1.760859644e11; %rad*Hz/T
        gam = 35.19e6; %Hz/(kA/m)
        %...in Gaussian units
        gamGauss = 2.802495164e6; %Hz/Oe
        %Boltzmann constant
        kB = 1.38064852*1e-23; %J/K
        %elementary charge 
        e = 1.602176634*1e-19;
        %electron g-factor
        ge = 2.0023193043617;
   end
   
end

