function y = calculate_Efun(x)
    
    %Necessary for calculating the electric field integral using
    %the Simpson method. The trancendental equation for a bilayer spherical resonator is used.
    
    global epsf epsd R1 R2 k mu1 mu2 omc
    N = length(x);
    t11 = sqrt(epsf.*mu1);
    t22 = sqrt(epsd.*mu2);
    k1 = omc.*t11;
    k2 = omc.*t22;
    af2 = k+0.5;
    x11 = k1.*R1;
    x21 = k2.*R1;
    x22 = k2.*R2;
    % Bessel function of 1st kind
    j2x11 = besselj(af2,x11);
    j2x21 = besselj(af2,x21);
    y2x21 = bessely(af2,x21);
    % Bessel function of 2nd kind
    j2x22 = besselj(af2,x22);
    y2x22 = bessely(af2,x22);
    %matrix
    A = mu1.*(k1.*R1).^0.5.*j2x11./((k2.*R1).^0.5.*(j2x21-j2x22./y2x22.*y2x21));
    %E_phi		
    y = zeros(1,N);

    for i=1:N

        ax = x(i);
        if (ax == 0)
            continue;
        end
        xi1 = k1.*ax;
        xi2 = k2.*ax;
        if (ax <= R1)
            ji = besselj(af2,xi1);
            y(i) = mu1.*(xi1).^0.5.*ji./ax;
            y(i) = y(i)*omc*PhysConst.c*PhysConst.mu0;
            %added a factor of 2 to E^2, trig function missing in formulas 
            y(i) = (abs(y(i))*ax)^2;
        else
            jk = besselj(af2,xi2);
            yk = bessely(af2,xi2);
            y(i) = (xi2).^0.5.*A.*(jk-j2x22./y2x22.*yk)./ax;
            y(i) = y(i)*omc*PhysConst.c*PhysConst.mu0;
            %added a factor of 2 to E^2, trig function missing in formulas 
            y(i) = (abs(y(i))*ax)^2;
        end   
        
    end

end

