% Computations of complex resonance frequencies and quality factors of modes
% in a sphere made of a ferromagnetic material located in the free space.
% Supplements "Ferromagnetic Resonance Revised � Electrodynamic Approach"

clear variables
close all
format long
global H0r R epsf k alpha Ms gam
k = 1;
%optimization settings
opt = optimset('TolFun',1e-14,'TolX',1e-14,'Display','off'); %<----------------------------------SET
%relative permittivity of the sphere
epsf = 16; %<----------------------------------SET
%radius of the sphere [mm]
R = 0.5; %<----------------------------------SET
%unit conversion mm -> m
R = R/1e3;
%gyromagnetic ratio [MHz/(kA/m)]
gam = 35.19; %<----------------------------------SET
%saturation magnetization [kA/m]
Ms = 140; %<----------------------------------SET
%frequency normalization factor
fm=gam*Ms*1e6;
%number of magnetic field values 
N = 50; %<----------------------------------SET
%range of the normalized (with respect to Ms) internal static magnetic field
Hstart = 1; Hstop = Hstart+5; %<----------------------------------SET
%ferromagnetic linewidth [Oe]
deltaH = 0.5; %<----------------------------------SET
%unit conversion Oe - > kA/m
deltaH = deltaH / (4*pi);
%starting point of the search for minima in the complex frequency domain
x0 = (Hstart+1/3)*2*pi*fm/PhysConst.c*[1 .001]; %<---------------------------SET

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%program switches%%%%%%%%%%%%%%%%%%%%
%plot the dependence of the quality factor on the magnetic field
showQvsH = true;  %<----------------------------------------------------SET
%plot the quality factor that neglects radiation losses
showQnorad = true;
%plot the dependence of the resonance frequency on the magnetic field
showFvsH = true;  %<----------------------------------------------------SET
%if true, the resonance frequency and internal static magnetic field will be normlized
normalize = true;
saveData = true; %<----------------------------------------------SET
%data will be saved to the following file
fileName1 = 'results_unshielded.txt';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%program switches%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%plot options%%%%%%%%%%%%%%%%%%%%%%%
%figure defaults can be set here
setFigureDefaults();
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%plot options%%%%%%%%%%%%%%%%%%%%%%%

xstart = x0;
%vector of relative magnetic fields
H = linspace(Hstart,Hstop,N);
%vector of Gilbert damping factors
alpha_vec = deltaH/(2*Ms)./H; 
%resonance frequency vector
frq = zeros(1,N);
%quality factor (complex frequency method)
Q = zeros(1,N);

for n = 1:N
    
    H0r = H(n);    
    alpha = alpha_vec(n);
    %determining the complex resonance frequency
    zer = fsolve(@TDE_unshielded,xstart,opt);
    %renormalization
    frq(n) = zer(1)*PhysConst.c/(2*pi);
    %Quality factor calculated as Re(zer)/(2*Im(zer))
    Q(n) = 0.5*zer(1)/zer(2); 
    %setting the new starting point
    xstart = zer;
    
end

%quality factor - neglect of radiation losses
Qnorad = Ms * H / deltaH;
%vector of normalized frequencies
w = frq/fm;

if (showFvsH)
    figure(); 
    if (normalize)
        plot(H,w,'.--'); 
        xlabel('Normalized magnetic field \it{H_{0r}}'); ylabel('Normalized resonance frequency, \it{w}');
    else
        plot(H*Ms,frq/1e9,'.--'); 
        xlabel('Magnetic field \it{H_{0}} \rm{[kA/m]}'); ylabel('Resonance frequency, \it{f} \rm{[GHz]}');
    end
end

if (showQvsH)
    
    figure();
    if (normalize)
        plot(H,Q,'.--'); 
        xlabel('Normalized magnetic field, \it{H_{0r}}');
    else
        plot(H*Ms,Q,'.--');
        xlabel('Magnetic field, \it{H_{0}} \rm{[kA/m]}');
    end     
    ylabel('Quality factor, \it{Q}');
    
    if (showQnorad)
        hold on;
        s = sprintf('\\it{\\DeltaH} \\rm{= %.2f Oe}', deltaH*4*pi);
        title(s);
        if (normalize)
            plot(H,Qnorad,'-');
        else
            plot(H*Ms,Qnorad,'-');
        end
        
        legend('magnetic + radiation losses', 'magnetic losses','Location','best');
    else
        title('magnetic + radiation losses');
    end
     
end

dat = [H' w' Q' Qnorad'];
%comment marker '#' included
dataHeaders = {'#H0r ','w ','QComplexFreq ','Qnorad '};
fileID = fopen(fileName1,'w');
fprintf(fileID,'%s',dataHeaders{:});
fprintf(fileID,'\r\n');
fclose(fileID);
save(fileName1, 'dat','-ascii','-append');


