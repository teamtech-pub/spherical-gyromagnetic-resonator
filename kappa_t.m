function y = kappa_t(Hr,w,alpha)
    y = w ./ (Hr.^2 - w.^2 + 2*1i*alpha.*Hr.*w);
end
