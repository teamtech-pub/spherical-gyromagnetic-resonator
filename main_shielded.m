% computations of the complex resonance frequencies 
% and e-m fields for a spherical cavity of radius R2 
% containing a spherical sample of radius R1, permittivity eps1 and
% permeability mu1
% The sample is immersed in a medium of permittivity eps2 and
% permeability miu2
% UPDATE: the characteristic equation can be chosen as a variable
clear variables
close all
format long
%global epsd sigma mu2 mu3 R1 R2 R3 k H0r alpha gam Ms K orientation
global re_eps1 re_eps2 re_eps3 sigma3 mu2 mu3 R1 R2 R3 n g Ms Hani low_loss K
%optimization settings
sigma3 = 6e7; %S/m
metal_thickness = 5;%um
g = 2.0443;
%
%opt = optimset('PlotFcns','optimplotfval','TolFun',1e-16,'TolX',1e-16,'MaxFunEvals',2000,'MaxIter',2000); %<----------------------------------SET
opt = optimset('TolFun',1e-17,'TolX',1e-17,'MaxFunEvals',20000,'MaxIter',20000);
magnon_mode = false; %%computing direct magnon mode or coupled with the cavity?
%gyromagnetic ratio [MHz/(kA/m)]
%gam = 35.19; %<----------------------------------SET
gam = PhysConst.gamGauss / PhysConst.ge * g * 1e-6 * 4 * pi;
%saturation magnetization [kA/m]
Ms = 30.239; %<----------------------------------SET
%first-order cubic anisotropy constant
K = 0; %erg / cm^3
%radius of the inner sphere [mm]
%orientation: h - hard, e - easy
orientation = 'e';
R1 = 0.5415; %<----------------------------------SET
%TEk01 mode frequency of the empty cavity (GHz)
f0 = 10.04475;
%ferromagnetic linewidth [Oe]
deltaH = 35; %<----------------------------------SET
%relative permittivity of the inner sphere
re_eps1 = 16.0; %<----------------------------------SET
%relative permittivity of the outer sphere
epsd = 1; %<----------------------------------SET
%relative permeability of the outer sphere
mu2 = 1.0; %<----------------------------------SET
%relative permeability of the metal layer
mu3 = 1.0; %<----------------------------------SET
% k denotes the degree of Legendree polynomial and order of the Bessel function 
% k is in fact the mode azimuthal number n
k = 1; %<----------------------------------SET
p = 2; %radial mode index p
%radius of the outer sphere [mm]
R2 = 1e3*outerSphereRadius(k,p,f0,1e-4); %<----------------------------------SET
R3 = R2 + metal_thickness;
%R2 = 2.5*10;
%number of magnetic field values 
NN = 200; %<----------------------------------SET
%number of spatial points (between 0 and R2)
Nk = 4001; %<----------------------------------SET
%range of the normalized (with respect to Ms) internal static magnetic field
Hstart = 7.5; Hstop = Hstart+2; %<----------------------------------SET
%frequency normalization factor
fm = gam*Ms*1e6;
%field offset due to anisotropy
%offset = -4/3*K/Ms; %(erg / cm^3) / (emu / cm^3) = Oe
%starting point of the search for minima in the complex frequency domain
if (magnon_mode)
    if (k == 2)
        x0 = (Hstart+2/5)*2*pi*fm*(R3/1000)/PhysConst.c*[1 .001]; %<---------------------------SET
    else
        x0 = (Hstart+1/3)*2*pi*fm*(R3/1000)/PhysConst.c*[1 .001]; %<---------------------------SET
    end
else
    if (k == 1)
        x0 = f0*1e9*2*pi*(R3/1000)/PhysConst.c*[1 .0001];
    else
        warning('Case not supported');
    end
end

%x0 = 8.7*2*pi*fm*(R2/1000)/PhysConst.c*[1 .001]; %<---------------------------SET
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%program switches%%%%%%%%%%%%%%%%%%%%
%methods of quality factor calculation
calculateQLaplace = true; %<----------------------------------------------------SET
calculateQEnergy = true; %<----------------------------------------------------SET
%method of energy calculation: 
%1 - imag, 1st derivative (original method)
%2 - imag, 2nd derivative 
%3 - imag, 3rd derivative
%4 - real, 1st derivative
%5 - real, 2nd derivative
%6 - real, 3rd derivative
calculateQEnergyMethod = 1; %<----------------------------------------------------SET
%plot the quality factor vs the static internal magnetic field (common plot)
showQvsH = true;  %<----------------------------------------------------SET
%plot the dependence of the resonance frequency on the magnetic field
showFvsH = true;  %<----------------------------------------------------SET
%if true, the resonance frequency and the internal static magnetic field will be normlized
normalize = true; %<---------------------------SET
%magnetic field indices for which to plot and save the computed e-m fields
showFieldsForIt = [1,NN]; %<----------------------------------------------SET
%if false, Simpson integration will be used
integrateTrapezoidal = true; %<----------------------------------------------SET
%if true, quad will be used, else integrate (available from Matlab 2012a)
intquad = false; %<----------------------------------------------SET
saveData = true; %<----------------------------------------------SET
%data will be saved to the following files
fileName2 = 'fields.txt'; %prepended with the number of iterations
fileName1 = 'Qc_mystery_trilayer';
%use the low-loss approximation when calculating energies (alpha^2 = 0)
low_loss_approximation = true; %<---------------------------SET
%debug mode (more plots)
debug = true; %<---------------------------SET
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%program switches%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%plot options%%%%%%%%%%%%%%%%%%%%%%%
%plot and figure default settings
setFigureDefaults(10,2,8,0);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%plot options%%%%%%%%%%%%%%%%%%%%%%%

xstart = x0;
H = linspace(Hstart,Hstop,NN);
%unit conversion Oe -> kA/m
deltaH = deltaH / (4*pi);
%vector of Gilbert damping factors
alpha_vec = deltaH/(2*Ms)./H; 
%unit conversion mm -> m
R1 = R1 / 1e3;
R2 = R2 / 1e3;
R3 = R3 / 1e3;
%vector of spatial ponits
xx = linspace(0,R2,Nk); %not needed for trilayer: no fields will be drawn
delta_xx = xx(2) - xx(1);
%resonance frequencies
frq = zeros(1,NN); 
%normalized (with respect to fm) resonance frequencies 
ww = zeros(1,NN); 
%coefficients accounting for the frequency-dependent permeability
miuprim = zeros(1,NN); miub = zeros(1,NN);
%ratio of the magnetic and electric energy
En_ratio = zeros(1,NN);
%electric energy
We = zeros(1,NN);
%magnetic energy
Wm = zeros(1,NN);
%losses
Wloss = zeros(1,NN);
%vectors of integral values
intH1vec = zeros(1,NN);
intH2vec = zeros(1,NN);
%vector of division points between integrals
Ievec = zeros(1,NN);
%vector of imaginary frequencies
im_frq = zeros(1,NN);

%auxiliary vectors
zer1 = zeros(1,NN);
zer2 = zeros(1,NN);

%real part of mu_r evaluated for real w
mur_w_real = zeros(1,NN);
%derivative of -omega*mur''
mubis_w_der = zeros(1,NN);
%total sum of integrals for real part
total = zeros(1,NN);
%Q evaluated from total
Qtotal = zeros(1,NN);
%second derivative of omega*mu'
mur_w_real_2nd = zeros(1,NN);
%second derivative of -omega*mu''
mur_w_imag_2nd = zeros(1,NN);
%quality factor computed from imaginary part equation, Taylor 2nd order
Q2nd = zeros(1, NN);
%vector for omega'' solved using quadric equation
omega_bis = zeros(1,NN);
%...solved using cubic equation
mur_w_real_3rd = zeros(1,NN);
mur_w_imag_3rd = zeros(1,NN);
if (calculateQLaplace) 
    Q = zeros(1,NN); 
end
if (calculateQEnergy)
    Qen = zeros(1,NN);
    Qc = zeros(1,NN);
end

%point on the surface of the magnetic sphere
Ie = round(R1 / R2 * (Nk - 1)) + 1;

%check orientation input
if (orientation(1) == 'h')
    K = 0; %overwrite anisotropy value because it has no effect for the hard orientation
elseif (orientation(1) == 'e')
    %do nothing
else
    error('Invalid orientation chosen');
end

for k1=1:NN
    
    alpha = alpha_vec(k1);
    H0r = H(k1);
    
    %determining the complex resonance frequency
    zer = fminsearch(@TDE_shielded_trilayer_fast,xstart,opt);
    zer1(k1)=zer(1);
    zer2(k1)=zer(2);
    %real part of resonant frequency
    frq(k1) = PhysConst.c*zer(1)/(2*pi*R3);
    %imaginary part of resonant frequency
    im_frq(k1) = PhysConst.c*zer(2)/(2*pi*R3);
    %Quality factor calculated as Re(zer)/(2*Im(zer))
    if (calculateQLaplace) 
        Q(k1) = (zer(1)/(2*zer(2)));
    end
    %step = 1e-1;
    %[h,Z] = visualise2Dfun(@TDE_shielded_trilayer_wrapper,zer(1)*(1-step),zer(1)*(1+step),500,zer(2)*(1-step),zer(2)*(1+step),500,'re','im','val','lin','lin','log',[]);
    %setting the next search starting point
    if k1<=20000
        xstart = zer;  
    else
        xstart=[(2*zer1(k1)-zer1(k1-1))   (2*zer2(k1)-zer2(k1-1))];
    end
    %computation of electric and magnetic field distributions
    %[E_phi, H_r, H_theta] = Ecx(xx)
    [yy,zz,vv] = calculate_fields(xx);
    yy(1) = 0; %necessary since calculate_fields returns NaN here
    vv(1) = vv(2);
    zz(1) = zz(2);
    yyr = real(yy);
    yyi = imag(yy);
    zzr = real(zz);
    zzi = imag(zz);
    vvr = real(vv);
    vvi = imag(vv);
    %normalization
    may = max(abs(yyr));
    %integrate into plots later
    yyr_disp = yyr / max(abs(yyr)); 
    yyi_disp = yyi / max(abs(yyi));
    zzr_disp = zzr/zzr(1); 
    zzi_disp = zzi/zzi(1);
    vvr_disp = vvr/vvr(1); 
    vvi_disp = vvi/vvi(1);

    % if iteration index k1 corresponds to any of the iteration
    % numbers in showFieldsForIt, then plot all fields
    if (any(k1==showFieldsForIt)) 

        if (normalize)
            s = sprintf('\\it{H_{0r}} \\rm{= %.2f}', H0r);
        else
            s = sprintf('\\it{H_{0}} \\rm{= %.2f kA/m}', Ms*H0r);
        end
        %distance [mm]
        distance = xx*1000;
        %xlabel
        xla = 'Radial distance \it{r} \rm{(mm)}';
        %E_phi
        figure(); plot(distance,yyr_disp,distance,yyi_disp);
        xlabel(xla); ylabel('Normalized electric field \it{E_{\phi}}');         
        legend('Real part', 'Imaginary part', 'Location','best'); title(s);            
        %H_theta
        figure(); plot(distance,vvr_disp,distance,vvi_disp); 
        xlabel(xla); ylabel('Normalized magnetic field \it{H_{\theta}}');          
        legend('Real part', 'Imaginary part', 'Location','best'); title(s);         
        %H_r
        figure(); plot(distance,zzr_disp,distance,zzi_disp);             
        xlabel(xla); ylabel('Normalized magnetic field \it{H_{r}}'); 
        legend('Real part', 'Imaginary part', 'Location','best'); title(s);
        
        %save shown fields
        %comment marker '#' included
        fieldHeaders = ['#r (m) ', 'Ephi ','Hr ','Htheta '];
        fileName = strcat(num2str(k1),'_',fileName2);
        fileID = fopen(fileName,'w');
        str = sprintf('%s\r\n',fieldHeaders);
        fprintf(fileID,str);
        fclose(fileID);
        plt = [xx' yyr_disp' zzr_disp' vvr_disp'];
        save(fileName, 'plt','-ascii','-append');

    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%field integration%%%%%%%%%%%%%%%%%%%%%%
    %
    %complex mu + kappa for real frequency
    %normalized resonance frequency
    ww(k1)=frq(k1)/fm;
    %coefficients accounting for the frequency dependent permeability
    %low-loss assumption
    if (low_loss_approximation)
        w = ww(k1);
        H0 = H(k1);
        mur_w_real(k1) = 1 + 1./(H0 - w);
        miub(k1) = w*alpha/(H0-w)^2;
        miuprim(k1) = 1 + H0/(H0-w)^2;
        mubis_w_der(k1) = 2*w*H0*alpha ./ (H0 - w).^3;
        mur_w_real_2nd(k1) = 2*H0 / (H0 - w)^3;
        mur_w_imag_2nd(k1) = 2*H0*(H0 + 2*w)*alpha ./ (H0 - w).^4;
        mur_w_real_3rd(k1) = 6*H0 / (H0 - w)^4;
        mur_w_imag_3rd(k1) = 12*H0*(H0 + w)*alpha / (H0 - w)^5;
    else
        w = ww(k1);
        %here w can be assigned to a complex frequency for experiments
        H0 = H(k1);
        
        mur_w_real(k1) = ((H0+(-1).*w).*(1+H0+(-1).*w).*(H0+w).^2+2.*H0.*(1+2.*H0).*w.^2.*alpha.^2) ...
        .*(H0.^4+w.^4+2.*H0.^2.*w.^2.*((-1)+2.*alpha.^2)).^(-1);
    
        miub(k1) = w.*(H0+w).^2.*alpha.*(H0.^4+w.^4+2.*H0.^2.*w.^2.*((-1)+2.*...
        alpha.^2)).^(-1);
    
        miuprim(k1) = ((H0+(-1).*w).^2.*(H0+w).^4.*(H0+H0.^2+(-2).*H0.*w+w.^2)+2.*H0.*w.^2.*( ...
        H0+w).^2.*(H0.^2.*(1+4.*H0)+(-2).*H0.*(1+4.*H0).*w+((-1)+4.*H0).*w.^2).* ...
        alpha.^2+8.*H0.^3.*(1+2.*H0).*w.^4.*alpha.^4).*(H0.^4+w.^4+2.*H0.^2.*w.^2.*(( ...
        -1)+2.*alpha.^2)).^(-2);
        
        mubis_w_der(k1) = 2.*H0.*w.*(H0+w).*alpha.*((H0+(-1).*w).*(H0+w).^3+4.*H0.*w.^3.*alpha.^2).*( ...
        H0.^4+w.^4+2.*H0.^2.*w.^2.*((-1)+2.*alpha.^2)).^(-2);
    
        mur_w_real_2nd(k1) = 2.*H0.*((H0+(-1).*w).^3.*(H0+w).^6+2.*w.*((-1).*H0+w).*(H0+w).^3.*(3.* ...
        H0.^4+4.*H0.^2.*w.^2+4.*H0.*w.^3+w.^4).*alpha.^2+8.*H0.^2.*w.^3.*(H0.^4+( ...
        -2).*H0.*w.^3+(-3).*w.^4).*alpha.^4).*(H0.^4+w.^4+2.*H0.^2.*w.^2.*((-1)+ ...
        2.*alpha.^2)).^(-3);
        
        mur_w_imag_2nd(k1) = 2.*H0.*alpha.*((H0+(-1).*w).^2.*(H0+w).^6.*(H0+2.*w)+(-4).*H0.*w.^2.*(H0+w) ...
        .^2.*(3.*H0.^4+(-4).*H0.^3.*w+2.*H0.^2.*w.^2+3.*w.^4).*alpha.^2+16.* ...
        H0.^3.*w.^6.*alpha.^4).*(H0.^4+w.^4+2.*H0.^2.*w.^2.*((-1)+2.*alpha.^2)).^(-3);
        
        mur_w_real_3rd(k1) = 6.*H0.*((H0+(-1).*w).^4.*(H0+w).^8+(-2).*(H0+(-1).*w).^2.*(H0+w).^4.*( ...
        H0.^6+6.*H0.^5.*w+13.*H0.^4.*w.^2+8.*H0.^3.*w.^3+13.*H0.^2.*w.^4+6.*H0.* ...
        w.^5+w.^6).*alpha.^2+16.*H0.^2.*w.^2.*(3.*H0.^8+4.*H0.^7.*w+3.*H0.^6.* ...
        w.^2+(-4).*H0.^4.*w.^4+3.*H0.^2.*w.^6+4.*H0.*w.^7+3.*w.^8).*alpha.^4+( ...
        -32).*H0.^4.*w.^4.*(H0.^4+w.^4).*alpha.^6).*(H0.^4+w.^4+2.*H0.^2.*w.^2.*(( ...
        -1)+2.*alpha.^2)).^(-4);
    
        mur_w_imag_3rd(k1) = 12.*H0.*(H0+(-1).*w).*(H0+w).*alpha.*((H0+(-1).*w).^2.*(H0+w).^8+(-8).*H0.* ...
        w.*(H0+w).^2.*(H0.^6+H0.^5.*w+3.*H0.^4.*w.^2+(-2).*H0.^3.*w.^3+3.* ...
        H0.^2.*w.^4+H0.*w.^5+w.^6).*alpha.^2+16.*H0.^3.*w.^3.*(2.*H0.^4+H0.^3.*w+ ...
        2.*H0.^2.*w.^2+H0.*w.^3+2.*w.^4).*alpha.^4).*(H0.^4+w.^4+2.*H0.^2.*w.^2.*( ...
        (-1)+2.*alpha.^2)).^(-4);
    
    end
    
    Ievec(k1) = Ie;
    if (integrateTrapezoidal)

        Efun = (yyr.*yyr+yyi.*yyi).*xx.*xx;
        intE1 = (Efun(1)+2*sum(Efun(2:Ie-1))+Efun(Ie))/2;
        intE2 = (Efun(Ie)+2*sum(Efun(Ie+1:Nk-1))+Efun(Nk))/2;
        Htfun = (vvr.*vvr+vvi.*vvi).*xx.*xx;
        Hrfun = (zzr.*zzr+zzi.*zzi).*xx.*xx;
        Hfun = Htfun+Hrfun/k/(k+1);
        intH1 = (Hfun(1)+2*sum(Hfun(2:Ie-1))+Hfun(Ie))/2;
        intH2 = (Hfun(Ie)+2*sum(Hfun(Ie+1:Nk-1))+Hfun(Nk))/2;
        intPwalls = Htfun(end) * calculateSurfaceResistance(1e-9*frq(k1),1,sigma);
        
        %trying to compute the Q-factor from the real part of
        %Poynting equation
%         I1 = PhysConst.mu0*((Hfun(Ie)+2*sum(Hfun(Ie+1:Nk-1))+Hfun(Nk))/2 +...
%             mur_w_real(k1)*(Hfun(1)+2*sum(Hfun(2:Ie-1))+Hfun(Ie))/2);
%         I2 = PhysConst.mu0*mubis_w_der(k1)*(Hfun(1)+2*sum(Hfun(2:Ie-1))+Hfun(Ie))/2;
%         I3 = (intE1+intE2)*PhysConst.eps0;
%         I4 = 0; %no electric losses
%         total(k1) = (I1 + im_frq(k1)/frq(k1) * I2) / I3;
%         Qtotal(k1) = 0.5*I2 / (I3 - I1);
        %trying to compute the Q-factor from the real part of
        %Poynting equation
        
        %trying to compute the Q-factor from the imaginary part of the
        %Poynting equation, with Taylor expansion to the second derivative
        
        IH1 = PhysConst.mu0*intH1*miub(k1); %losses only is sphere
        IH2 = PhysConst.mu0*(miuprim(k1)*intH1 + intH2); %omega''
        IH3 = PhysConst.mu0*0.5*mur_w_imag_2nd(k1)*intH1;%omega''^2
        IH4 = PhysConst.mu0*(1/6)*intH1;%omega''^3, outisde sphere third derivative is 0
        IE1 = 0; %no electric losses
        IE2 = (intE1*real(re_eps1)+intE2*real(epsd))*PhysConst.eps0; %no epsilon dispersion
        IE3 = 0; %no electric losses
        IE4 = 0; %no epsilon dispersion
        I1 = PhysConst.mu0*miub(k1)*(Hfun(1)+2*sum(Hfun(2:Ie-1))+Hfun(Ie))/2;
        I2 = PhysConst.mu0*(miuprim(k1)*(Hfun(1)+2*sum(Hfun(2:Ie-1))+Hfun(Ie))/2 + ...
            (Hfun(Ie)+2*sum(Hfun(Ie+1:Nk-1))+Hfun(Nk))/2); %omega''
        I3 = PhysConst.mu0*0.5*mur_w_imag_2nd(k1)*(Hfun(1)+2*sum(Hfun(2:Ie-1))+Hfun(Ie))/2; %omega''
        I4 = 0;
        I5 = (intE1+intE2)*PhysConst.eps0; %no electric losses
        %I6 = 
        tmp = roots([I3 I2+I5 -w*(I1 + I4)]);
        if (tmp(1) > 0)
            omega_bis(k1) = tmp(1);
        else
            omega_bis(k1) = tmp(2);
        end
        Q2nd(k1) = w./omega_bis(k1)/2;
        %trying to compute the Q-factor from the imaginary part of the
        %Poynting equation, with Taylor expansion to the second derivative
        
    else %Simpson method
       
        if (intquad)
            intE1 = quad(@calculate_Efun,xx(1),xx(Ie),1e-3); 
            intE2 = quad(@calculate_Efun,xx(Ie),xx(Nk),1e-3);
            intH1 = quad(@calculate_Hfun,xx(1),xx(Ie),1e-3);
            intH2 = quad(@calculate_Hfun,xx(Ie),xx(Nk),1e-3);
        else
            intE1 = integral(@calculate_Efun,xx(1),xx(Ie)); 
            intE2 = integral(@calculate_Efun,xx(Ie),xx(Nk));
            intH1 = integral(@calculate_Hfun,xx(1),xx(Ie));
            intH2 = integral(@calculate_Hfun,xx(Ie),xx(Nk));
        end

    end
    
    if (debug)
        intH1vec(k1) = abs(real(miuprim(k1)))*intH1;
        intH2vec(k1) = mu2*intH2;
    end
    Wloss(k1) = PhysConst.mu0*intH1.*abs(real(miub(k1)));
    Wm(k1) = (intH1*abs(real(miuprim(k1)))+intH2*mu2)*PhysConst.mu0; %magnetic energy
    We(k1) = (intE1*real(re_eps1)+intE2*real(epsd))*PhysConst.eps0; %electric energy
    %Energy ratio 
    En_ratio(k1) = Wm(k1)/We(k1);
    %quality factor
    if (calculateQEnergyMethod == 1)
        Qen(k1) = (Wm(k1)+We(k1))/Wloss(k1)/2;
        Qc(k1) = delta_xx * 2*pi* frq(k1)*(Wm(k1)+We(k1)) / intPwalls / 2;
    elseif (calculateQEnergyMethod == 2)
        tmp = roots([IH3+IE3 IH2+IE2 -w*(IH1 + IE1)]);
        if (tmp(1) > 0)
            omega_bis(k1) = tmp(1);
        else
            omega_bis(k1) = tmp(2);
        end
        Qen(k1) = (Wm(k1)+We(k1))/Wloss(k1)/2;
    elseif (calculateQEnergyMethod == 3)
        
        Qen(k1) = (Wm(k1)+We(k1))/Wloss(k1)/2; % the stored energy needs a factor of 0.5, dont know why
    end
    %%%%%%%%%%%%%%%%%%%%%%%%field integration%%%%%%%%%%%%%%%%%%%%%%    
end
   
dat = [H',ww'];

if (showFvsH)
    figure(); 
    if (normalize)
        plot(H,ww,'.--'); 
        xlabel('Normalized magnetic field \it{H_{0r}}'); ylabel('Normalized resonance frequency \it{w}');
    else
        plot(H*Ms,frq/1e9,'.--'); 
        xlabel('Magnetic field \it{H_{0}} \rm{(kA/m)}'); ylabel('Resonance frequency \it{f} \rm{(GHz)}');
    end
end

%saving data

if (calculateQLaplace)
    dat = [dat Q'];
end

if (calculateQEnergy)
    dat = [dat Qen'];
end

if (showQvsH)
    
    figure();
    if (normalize)
        xla = 'Normalized magnetic field \it{H_{0r}}';
    else
        xla = 'Magnetic field \it{H_{0}} \rm{(kA/m)}';
    end
    if (calculateQLaplace)
        
        if (normalize)
            plot(H,Q,'.--'); 
        else
            plot(H*Ms,Q,'.--');
        end
        
        xlabel (xla); ylabel('Quality factor \it{Q}');
        set(gca, 'YScale', 'log')
        if (calculateQEnergy)
            hold on;
        else
            s = sprintf('Laplace method');
            title(s);
        end
        
    end
    if (calculateQEnergy)
       
        if (normalize)
            plot(H,Qen,'r.--');
        else
            plot(H*Ms,Qen,'r.--');
        end
        xlabel(xla); ylabel('Quality factor \it{Q}');  
        if (calculateQLaplace)
             legend('Laplace method','Energy integration','Location','best');
            
        else
             s = sprintf('Energy integration');
             title(s);
        end
        
    end
end

if (debug)
    figure; plot(H,100*abs(Qen-Q)./Q);
    xlabel(xla);
    ylabel('Relative error in Q-factor (%)');
    set(gca,'Yscale','log');
    figure; plot(H,En_ratio); 
    ylabel('Energy ratio (W_m/W_e)');
    xlabel(xla);
    set(gca,'Yscale','log');
    figure; plot(H,Wm); set(gca,'Yscale','log'); title('Magnetic energy');
    figure; plot(H,We); set(gca,'Yscale','log'); title('Electric energy');
    figure; subplot(1,2,1); plot(H,real(miuprim)); 
    subplot(1,2,2); plot(H,imag(miuprim));
    set(gca,'Yscale','log');
    setFigureDefaults();
    figure; hold on;
    plot(H,abs(mur_w_real),'--','Linewidth',6);
    plot(H,miuprim); 
    plot(H,miub);
    mur_vec = mu_t(H,ww+1i*im_frq/fm,alpha_vec) + kappa_t(H,ww+1i*im_frq/fm,alpha_vec);
    plot(H,abs(real(mur_vec)));
    plot(H,-imag(mur_vec));
    legend('|real(\mu_r)|','d(\omega\mu_r'')/d\omega','imag(\mu_r)',...
        '|real(\mu_r)| - Laplace','imag(\mu_r) - Laplace','Location','northwest');
    set(gca,'Yscale','log');
    xlabel('Normalized magnetic field \it{H_{0r}}');
    figure; plot(H,Q2nd); hold on;
    plot(H,Qen)
    set(gca,'Yscale','log');
    plot(H,Q)
    legend('Taylor 2nd', 'Taylor 1st', 'Laplace');
end

if (saveData)
    %other data to be saved
    dataHeadersFull = {'#H0r ','w ','QLaplace ','QEnergy '};
    for i = 1:size(dat,2)
        dataHeaders{i} = dataHeadersFull{i};
    end
    
    fileNameAppendix = strcat('_n=',num2str(k),'_epsd=',num2str(epsd),'_reeps1=',num2str(re_eps1),'_R2=',...
        num2str(R2*1e3),'_Ms=',num2str(Ms),'_K=',num2str(K),'_dH=',num2str(deltaH*4*pi),'.txt');
    fileID = fopen(strcat(fileName1,fileNameAppendix),'w');
    fprintf(fileID,'%s',dataHeaders{:});
    fprintf(fileID,'\r\n');
    fclose(fileID);
    save(strcat(fileName1,fileNameAppendix), 'dat','-ascii','-append');

end