function [] = setFigureDefaults(afs,lw,ms,g)
    
    set(0,'defaultfigurecolor',[1 1 1]);
    set(0,'DefaultLineMarkerSize',ms);
    set(0,'DefaultAxesFontSize',afs);
    if (g)
        set(0,'DefaultAxesXGrid','on','DefaultAxesYGrid','on');
    else
        set(0,'DefaultAxesXGrid','off','DefaultAxesYGrid','off');
    end
    set(0,'defaultLineLineWidth',lw);

end