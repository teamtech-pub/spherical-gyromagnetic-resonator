function Rs = calculateSurfaceResistance(f,mur,sigma)
%f in GHz

    Rs = sqrt(pi*f*1e9*mur*PhysConst.mu0/sigma);


end

